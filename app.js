//[SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const userRoutes = require('./routes/users');
	const courseRoutes = require('./routes/courses');
	const cors = require('cors');

//[SECTION] Environment Setup
	dotenv.config();
	let account = process.env.CREDENTIALS;
	const port = process.env.PORT;

//[SECTION]	Server Setup
	const app = express();
	app.use(express.json());
	//it enables all origins/address/URL of the client requests
	app.use(cors());

//[SECTION]	Database Connection
	mongoose.connect(account);
	const connectStat = mongoose.connection;
	connectStat.once('open', () => console.log('Database Connected'));

//[SECTION]	Backend Routes
	app.use('/users',userRoutes);
	app.use('/courses', courseRoutes);

//[SECTION]	Server Gateway Response
	app.get('/', (req,res) => {
		res.send(`Welcome to Course-Bruh!`);
	});
	app.listen(port, () => {
		console.log(`API is Hosted on port ${port}`);
	});
