//[SECTION] Dependencies and Modules
	const Course = require('../models/Course')
//[SECTION]Environment Variable Setup
//[SECTION] Functionalities [CREATE]
	/*
		Steps:
		1. Create a new Course object using the mongoose model and the information from the request body.
		2. Save the new Course to the DB.	
	*/

	module.exports.addCourse = (reqBody) => {
		//create a variable "newCourse" and instantiate the name, description, price.
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		});

		//Save the created object to our DB.
		return newCourse.save().then((course, error) => {
			//Course creation is successful or not
			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error);
	};

//[SECTION] Functionalities [RETRIEVE]
	//Retrieve all courses from the DB
	module.exports.getAllCourses = () => {
		return Course.find({}).then(result => {
			return result;
		});
	};


	//Retrieve all active courses
	module.exports.getAllActive = () => {
		return Course.find({ isActive: true }).then(result => {
			return result;
		}).catch(error => error);
	};

	//Retrieve a course
	module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result
	}).catch(error => error);
};

//[SECTION] Functionalities [UPDATE]
	//update a course
		/*
			Steps:
			1.Create a variable "updatedCourse" which will contain the info retrieved from the req.body.
			2. find and update the course using the courseId retrieved from the req.params and the variable "updatedCourse" containing info from req.body. 
		*/

	//Specify the fields/properties of the documents to be updated.
	module.exports.updateCourse = (courseId, data) => {
		let updatedCourse = {
			name: data.name,
			description: data.description,
			price: data.price
		};

		//findByIdAndUpdated(document Id, updatesToBeApplied)
		return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error);
	};
	
//[SECTION] Functionalities [DELETE]
	//Archiving a course
		//1.update the status of "isActive" to false which will no longer be displayed in the client whenever all active courses are retrieved.

	module.exports.archiveCourse = (courseId) => {
		let updateActiveField = {
			isActive: false
		};

		return Course.findByIdAndUpdate(courseId, updateActiveField).then((course, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			}).catch(error => error)
		}

	//Activate a course
	module.exports.activateCourse = (courseId) => {
		let updateActiveField = {
			isActive: true
		};

		return Course.findByIdAndUpdate(courseId, updateActiveField).then((course, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			}).catch(error => error)
		}
 	

	