//[SECTION] Dependencies and Modules
	const express = require('express');
	const CourseController = require('../controller/courses');
	const auth = require('../auth');

//Destructure the actual function that we need to use.
	const {verify, verifyAdmin} = auth;

//[SECTION]	Routing Component
	const route = express.Router();

//[SECTION] Routes-POST
	route.post('/create', verify, verifyAdmin, (req, res) => {
		CourseController.addCourse(req.body).then(result => res.send(result));
	});

//[SECTION] Routes-GET
	//retrieve all courses
	route.get('/all', (req, res) => {
		CourseController.getAllCourses().then(result => res.send(result));
	});

	//retrieve all active courses
	route.get('/active', (req, res) => {
		CourseController.getAllActive().then(result => res.send(result));
	});

	//retrieve a specific course 
	//req.params (parameter)
	route.get('/:courseId', (req, res) => {
		console.log(req.params.courseId)
		//we can retrieve the courseId by accessing the request's params property which contains all the parameters providede via the url
		CourseController.getCourse(req.params.courseId).then(result => res.send(result));
	});

//[SECTION] Routes-PUT
	//update a course
	route.put('/:courseId', verify, verifyAdmin, (req, res) => {
		CourseController.updateCourse(req.params.courseId, req.body).then(result => res.send(result));
	});

//[SECTION] Routes-DEL
	//Archiving a course
	route.put('/:courseId/archive', verify, verifyAdmin, (req,res) => {
		CourseController.archiveCourse(req.params.courseId).then(result => res.send(result));
	});

	//Activate a course
	route.put('/:courseId/activate', verify, verifyAdmin, (req,res) => {
		CourseController.activateCourse(req.params.courseId).then(result => res.send(result));
	});



//[SECTION] Expose Route System
	module.exports = route;